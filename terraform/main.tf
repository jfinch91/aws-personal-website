variable "aws_key" {
  type = string
  description = "The name of the key pair for this AWS instance"
  sensitive = true
}

terraform {
  backend "http" {
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region     = "eu-west-2"
}

resource "aws_security_group" "mk8_sec_group" {
  name              = "mk8_sec_group"
  description       = "MicroK8 project security group"

  ingress {
    from_port       = 16443
    to_port         = 16443
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
    description     = "k8"
  }

  ingress {
    from_port       = 22
    to_port         = 22 
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
    description     = "ssh"
  }

  ingress {
    from_port       = 3389
    to_port         = 3389 
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
    description     = "rdp"
  }

  ingress {
    from_port       = 80
    to_port         = 80 
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
    description     = "http"
  }


  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "mk8_host" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.medium"
  key_name = var.aws_key
  security_groups = [ "mk8_sec_group" ]
  tags = {
    Name = "mk8_host"
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = 20
  }

  lifecycle {
    ignore_changes = [ami]
  }
}

resource "aws_eip" "lb" {
  instance = aws_instance.mk8_host.id
  vpc      = true
}



