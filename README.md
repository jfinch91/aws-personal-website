This project was created to learn about various different technologies, softwares and development tools. There have been a number of different phases each expanding and improving previous developments in the project. 

## Initial Goal
The initial goal of this project was to develop a basic personal website that was hosted on AWS, running in Kubernetes (MicroK8s). The provisioning of resources in AWS was to be managed by Terraform and their configuration/setup by Ansible. The code was to be stored in Gitlab and CI/CD pipelines setup to build the website, build as Docker image and deployed to K8 cluster, thus updating the website with new changes.

## Clone For Your Project 
Take a look at this [wiki](https://gitlab.com/jfinch91/aws-personal-website/-/wikis/Clone-the-Template-for-use-in-a-Project) page to setup the template for yourself 

## Technologies Used 
- AWS
- Terraform
- Ansible
- Kubernetes
- Gitlab
    - Code Repository
    - CI/CD Pipelines
- Docker
- React
- Node.JS
- Nginx
- Helm

## Phases

### Phase 1: Publicly accessible Nginx deployment in K8
Phase 1 sets up the hardware and its configuration with Terraform and Ansible running from my local machine but Gitlab-CI automating the deployment of Nginx to the K8 cluster. 
1. Terraform script for provisioning AWS instance, with security group for SSH, RDP, HTTP and K8 cluster and a Elastic IP This was run locally for initial setup.
    - [Install Terraform Locally](https://learn.hashicorp.com/tutorials/terraform/install-cli?in=terraform/aws-get-started)
    - [Install AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
    - Create AWS Access Key to [configure AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-quickstart.html)
    - Write .tf script for
        - EC2 on ubuntu
        - 20gb SSD
        - Security group with RDP, SSH and K8 ports enabled
        - Elastic IP
2. Ansible script to configure on AWS instance
    - [Install and configure Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-macos)
    - Ansible script to [install MicroK8 on host](https://ubuntu.com/tutorials/install-a-local-kubernetes-with-microk8s#1-overview)
    - Ansible script also [sets up RDP](https://linuxize.com/post/how-to-install-xrdp-on-ubuntu-20-04/) so we can remote desktop in and view K8 Dashboard
    - Configure MicroK8 certs with AWS instance IP (slight hack as MicroK8s doesn't support external access) [Example]( https://github.com/pfisterer/edsc-microk8s-playbook/blob/master/roles/microk8s/tasks/main.yaml)
3. Setup Gitlab repo
    - Create SSH key pair to enable committing to repo
4. Configure Gitlab CI to deploy changes to K8 cluster in AWS 
    - Store certificate, K8 access token in Gitlab variables to access cluster from Gitlab
    - Update .gitlab-ci.yml to use [dtzar/helm-kubectl](https://sanderknape.com/2019/02/automated-deployments-kubernetes-gitlab/) docker image, so we have kubectl available to deploy repo's kubernetes folder to K8 cluster in AWS
5. Define basic Nginx K8 deployment
    - Create K8 Nginx deployment definition
    - Define K8 service expose Nginx deployment
    - Define K8 Ingress to route to Nginx service

### Phase 2: Replace Nginx deployment with Dockerised React deployment
Phase 2 replaces the Nginx app with the template React app and adding the dockerisation of this app to its deployment to K8 with Gitlab-CI
1. Create react app locally
    - Locally install Node Version Manager (NVM) with brew
    - Use NVM to install Node and Node Package Manager (npm)
    - Template base typescript react app with `npx create-react-app my-app --template redux-typescript` 
2. 	Dockerise react app
	- Install Docker locally
	- Create Dockerfile for project and build docker container locally, test locally
3. Get gitlab to build docker image and deploy to K8 cluster 
    - Create Deploy Token in Gitlab so K8 cluster can pull images from repo
    - Update gitlab-ci.yml script to build [docker](https://dev.to/christianmontero/gitlab-ci-cd-example-with-a-dockerized-reactjs-app-1cda) image in Gitlab and push to gitlab container repo
    - Create react K8 deployment that pulls react app container from gitlab repo 
    - Update gitlab-ci.yml script to deploy to k8 cluster 

### Phase 3: Integrate Terraform and Ansible into Gitlab-CI and migrate K8 deployment to Helm
Phase 3 looks to fully automate the provisioning of hardware, hardware configuration and application deployment by integrating Terraform and Ansible stages to Gitlab-CI. Furthermore K8 deployment is migrated to use Helm as this allows for passing in sensitive information via environment varibles.
1. Integrate Terraform
    - Migrate the local backend of terraform setup to be in Gitlab's managed state [backend](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html#migrating-to-gitlab-managed-terraform-state). Need Gitlab access token with API role so Terraform can access backend in Gitlab
    - Update gitlab-ci with terraform template from Gitlab core project, add the predefined terraform stages init, validate, build and deploy.
    - Store AWS access keys in Gitlab variables so Terraform can access AWS account and provision resources
    - Update .tf script so no sensitive data is committed to repo
2. [Integrate Ansible](https://kruyt.org/ansible-ci-with-gitlab/)
    - Gitlab variables for AWS host, user and access key pair so it can configure it
    - Remove sensitive data in local ansible script ready for commit
    - In script save kubectl config so we have data source for access details and can save these in Gitlab variables as well for K8 deploy
    - Add ansible stages to gitlab-ci or verifying and running ansible playbook
3.  K8 Deployment using Helm
    - Create helm chart [template](https://opensource.com/article/20/5/helm-charts)
    - Edit template to generate the same manifest files as currently exist for K8 deployment
    - Save deployment token as gitlab variable which now can be passed in to Helm rather than having it visible in committed code.
    - Update gitlab-ci to use Helm CLI


### Future Work

Here I document potential future phases of the project. 

**Storing Secrets in Vault**

Secrets such has logins, access tokens can be stored in Gitlab environment variables and there are features in Gitlab that tries to protect them e.g. masking, access only on protected branches. However using Gitlab doesn't provide much functionality for restricting access and if someone gained access to the project it isn't that hard to expose the secrets even when they are masked ([REF](https://swiety-python.blogspot.com/2021/01/managing-secrets-in-gitlab.html)). 

Gitlab has integration with Hashicorp Vault which allows for the storage and control of secrets via CLI or API. Secrets are stored in Vault, where access is controlled via polices and roles. Gitlab CI jobs can authenticate with Vault via Json Web Tokens (JWT) to retrieve secrets. The JWT are created in the free tier of Gitlab but the automatic retrieve and saving of secrets in environment variables is only available in Premium and above. However you can still manually write a script in gitlab-ci.yml to retrieve secrets with JWT. 

Vault provides a much better environment for storing and accessing secrets but fundamentally as the Gitlab runner accesses the secrets, this can be changed to expose the secret if someone got access to the project who shouldn't have it. The best practise in this scenario is to have rotating credentials so that even if they were exposed, after a short amount of time the secret would be changed/updated giving more protection. Setting up this seems like a fair amount of work at this stage so documenting it for later.  

A future phase of the project could involve:
1. Setting up Vault in AWS with Ansible [Blog](https://medium.com/@mitesh_shamra/setup-hashicorp-vault-using-ansible-fa8073a70a56)
2. Create secrets in Vault [Docs1](https://docs.gitlab.com/ee/ci/secrets/) 
3. Configure roles and policies around these secrets [Docs2](https://docs.gitlab.com/ee/ci/secrets/)
4. Configure Gitlab CI runners to authenticate with Vault and retrieve secrets for use in build scripts [Docs3](https://holdmybeersecurity.com/2021/03/04/gitlab-ci-cd-pipeline-with-vault-secrets/)





